import React from 'react';
import LandingPage from './components/LandingPageComponent';
function App() {
  return (
    <div className="App">
      <LandingPage/>
    </div>
  );
}

export default App;
