import React,{Component} from 'react';
import {Button} from 'antd';
class LandingPage extends Component{
    render(){
        return(
            <div className="landing-page-container">
                <h1>Hello</h1>
                <Button type="primary">Primary</Button>
            </div>
        )
    }
}

export default LandingPage;
